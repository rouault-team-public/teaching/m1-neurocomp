---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.14.4
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

# Introduction to Python with Spiking neurons


## Introduction

Within this course, we will often refer to [this
page](https://docs.python.org/3/tutorial/index.html).
It is a really good source of information and it is the official Python page,
which means the information is uptodate and accurate.

If you would like to read more about python, you can find a good list of books
about Python [there](https://wiki.python.org/moin/PythonBooks) (not all of them
are free).

Here are two free online books that could be worth reading to go further:
- [IPython Cookbook, Second Edition (2018)](https://ipython-books.github.io/)
  In depth book about python. This book is of general interest and definitely
  worth reading
- [From Python to
  Numpy](https://www.labri.fr/perso/nrougier/from-python-to-numpy/) Especially
  useful when wanting to use numpy, which you will most certainly end up doing!


### Goal of this course
This course aims at teaching the basics of coding using spiking neurons (Leaky
integrate and fire) simulations as a support.

At the end of the course, we would like you to be able to write small pieces of
code to do basic data analysis.


### Coding in Python
The goal of this course is for you to learn the basics of coding in python?

#### What is coding?
Coding is giving a set of instructions to a computer for it to do a task.
The tasks can be as trivial as:
```
Give the result of 1 + 0
```
or, slightly more complex:
```
If the key 'a' from the keyboard is stroke, display the letter 'a' on the
screen
```
or quite complex:
```
Help proving the 4-colour theorem
```
(see [Computer-assisted proofs](https://en.wikipedia.org/wiki/Computer-assisted_proof))

#### Programming language
To communicate and give instructions to a computer and mainly the Computer
Processing Unit (CPU), it is necessary to write the said instructions in a
language that the CPU can indeed understand.

There is an extremely large number of programming languages (635 listed on this
[wikipedia page](https://en.wikipedia.org/wiki/List_of_programming_languages)).
For example C, C++, Java, R, Go, Rust...

In this class we will use Python as our language of choice.
This choice was mainly driven by three reasons:
- it is a language simple enough to learn (no type, elegant syntax, etc)
- it is an open source language
- it is probably the open source language that is used the most in
  biology and data science

Supporting the previous claims, the [TIOBE Programming Community index
rank](https://www.tiobe.com/tiobe-index/) ranks python in the top 3 of the
currently most popular programming languages.

#### Learning a programming language
Therefore, to code, you need to learn a programming language (here Python).
Programming languages are all at least somewhat different and have specific
rules but most of them rely on a common set of paradigms:
- they have variables
- they have data structures (list, heaps, hashmaps, ...)
- they have conditional statements
- they have functions (not always)

Before being able to write some code, it is important to go through these basic
and mostly common "rules"


### Leaky integrate and fire model

A more detailed tutorial on LIF neurons can be found on the neuromatch website
[Neuromatch -- Neuron
models](https://compneuro.neuromatch.io/tutorials/W2D3_BiologicalNeuronModels/chapter_title.html)
(the following section is adapted from that page, by Neuromatch,
licensed under CC BY, 4.0).

The LIF model captures the facts that a neuron:

- performs spatial and temporal integration of synaptic inputs
- generates a spike when the voltage reaches a certain threshold
- goes refractory during the action potential
- has a leaky membrane

The LIF model assumes that the spatial and temporal integration of inputs is
linear. Also, membrane potential dynamics close to the spike threshold are much
slower in LIF neurons than in real neurons.

The dynamical equation of the LIF neuron writes:

$$\tau_m \frac{dV}{dt}= −(V−E_L) + \frac{I}{g_L}$$
where:
- $V$ is the membrane potential
- $g_L$ is the leak conductance
- $E_L$ is the resting potential
- $𝐼$ is the external input current
- $\tau_m$ is the membrane time constant.
