---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.14.4
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---


## Some exercises
Now, you should have enough to go through a small batch of exercises 🥳 🥳 🥳.

### Exercise 3

First, we want to update the membrane potential without taking into account the
resettings.
Write a loop that increments the potential value according to the following
equation:
$$\delta V = \delta t (-V +E_L + R I_\mathrm{in}) / \tau_m$$
where $\delta V$ is the increment of potential.

You will vary the input current. Can you predict what will happen when we
introduce the resettings?

```python
tau_m = 10
dt = 1.0
V_0 = -25
E_L = -65
RI_in = 0

V = V_0
t=0
while t< 30:
    V += dt * (-V + E_L + RI_in) / tau_m
    t += dt
print(f'{V                            = }')
```

### Exercise 4
Now we have access to the last value of the voltage potential but we would like
to be able to access all the values in order to plot them.

To do so, write a piece of code that stores all the intermediary results in a
list.

V = V_0
t=0
while t< 30:
    V += dt * (-V + E_L + RI_in) / tau_m
    t += dt
    V_all.append(V)
print(f'Last value of V (V[-1])          --> {V_all[-1]}')
```

Given the list of concentrations, one can plot its evolution over time using
the function plot:
```python
from matplotlib import pyplot as plt

fig, ax = plt.subplots()
# ax.plot( values )
ax.plot(V_all)
```

### Exercise 5
Now that we know how to compute the evolution of the volage potential, we want
to take into account spike threshold and resettings. Can you implement that in
the loop?


```python
V_reset =
V_thr =
```

As before, it is possible to plot the values of the potential with the plot
function.

