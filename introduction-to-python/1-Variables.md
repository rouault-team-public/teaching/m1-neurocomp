---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.14.4
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

## Variables

Variables are symbolic names where information or values can be stored.

They are the cornerstone of coding, they allow you to store in memory values
and to access them later on. In our example the variables have been described
earlier (`tau_m`, `V`, etc).

To assign an information to a variable, the `=` operator is used.

> ⚠️ be careful, `=` is the assignment operator. To check the equality between
> two variables, the required operator is `==` ⚠️

For example after the following line of code is ran:
```python
a_number = 0
a_number = 10
another_number = 1
```

> Note: to run a code block within Jupyter Lab, simply press ⇧⏎. 

The variable `a_number` used to contain the value `0` and now contains the
value `10`.

The variable `another_number` contains the values `1`.

It is possible to display what is contained in a variable using the function
`print` for example:

```python
print(f'{a_number = }')
```

The content of a variable can be stored in another variable and then changed
without altering it:

```python
a_number = 5
another_number = a_number
a_number = 1
print(f'{another_number = }')
print(f'{a_number = }')
```

Variables can contain most (computational) things (especially in Python).
For example they can contain different types of data such as `list`,
`dictionary` or `ndarray` (we will see what they are right after)

Variables can also store the result of an operation:

```python
nb1 = 1
nb2 = 3
sum_nb1_2 = nb1 + nb2
print(f'{nb1 = }')
print(f'{nb2 = }')
print(f'{sum_nb1_2 = }')
```
> Note: in Python, variable are not "typed": you can assign an integer to a
> variable and then reassign this same variable with a string! This is
> different from most other programming languages.

### Exercises

#### Exercise 1
Set the value of the variables necessary for the model as follow:
- $E_L$: -65mV
- $\tau_m$: 10ms
- $g_L$: 50pS
- $V_\textrm{reset}$: -75mV
- $V_\textrm{thr}$: -40mV
- $dt$: 0.5ms
- $t_\textrm{fin}$: 2s

##### Write the answer to the previous question here:
```python

```

#### Exercise 2
Given a variable `nb1` and a variable `nb2`, put the values of each other
variables in the other one


```python tags=[]
import numpy as np

# Generate random numbers so you can't really cheat.
rng = np.random.default_rng()

nb1 = rng.integers(0, 5)
nb2 = rng.integers(6, 10)
print('before')
print(f'{nb1 = }')
print(f'{nb2 = }')

# TODO: swap a and b values

print('after:')
print(f'{nb1 = }')
print(f'{nb2 = }')
```

```python

```
