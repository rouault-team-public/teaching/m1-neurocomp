## 6. From 1 cell to N cells

Now, back to our cell dynamics!

We have seen how to model the voltage evolution for one neuron.


### 6.1 Representing a row of cells that are behaving independently.

First thing first, before playing with cell coupling, we will display a row of
cells that each have the previous small model embedded within but with a
different, random, starting point.

To do so, we will use a `ndarray`.
Our `ndarray` will have two dimensions.
The first dimension will be our spatial dimension, the second dimension will be the time.

To initialise an `ndarray` it is necessary to set its size. In our case, it
will be the number of cells `size` and the number of time points `n`.

### Exercise 9
Using the function `np.zeros` build an array named `V`
dimensions `size * n`


```python
# importing the numpy library
import numpy as np

# and carrying over the previously declared variables
```


```python
V = np.array([0])
print(f'V -->\n{V}\n')
print(f"""expected V -->
[[0. 0. 0. ... 0. 0. 0.]
 [0. 0. 0. ... 0. 0. 0.]
 [0. 0. 0. ... 0. 0. 0.]
 ...
 [0. 0. 0. ... 0. 0. 0.]
 [0. 0. 0. ... 0. 0. 0.]
 [0. 0. 0. ... 0. 0. 0.]]\n""")
print(f'V.shape          --> {V.shape}')
print(f'expected V.shape --> (100, 9000)')
```

This table represents the potential at each time-point.

For example `V[0, 100]` gives you the potential of the cell `0` at
time-point `100`. It is sometimes confusing to me whether the cell is first or
the gene concentration. One way to remember is to look at the `shape` of the
table (which is its dimension). Here the shape is `(100, 9000)`, so the first
component is the cell (since there are 100 of them).

### Exercise 10

Access the values of the 4 first time-points for the last 3 cells


```python
```

Now that you can access some places in your table, it is good to know that you
can also modify the values that you are accessing.

For example we saw previously that you can add values to an array, well you can
do so too for slices of an array:


```python
V = np.zeros((size, n))
print(f"V -->\n{V}\n")
print(f"V[:5, :5] -->\n{V[:5, :5]}\n")

# Adding 1 to the five first time points of the five first cells
V[:5, :5] = V[:5, :5] + 1
print('V[:5, :5] = V[:5, :5] + 1\n')
print(f"V[:5, :5] -->\n{V[:5, :5]}\n")
print(f"V -->\n{V}\n")
```

You can also use the `+=`, `*=`, `/=`, ... operators:


```python
V = np.zeros((size, n))
print(f"V[:5, :5] -->\n{V[:5, :5]}\n")

# Adding 1 to the five first time points of the five first cells
V[:5, :5] += 1

print(f"V[:5, :5] -->\n{V[:5, :5]}\n")
```

Not only you can add values but you can also assign the values of another array
(which is, if you think about it, what we actually already did with the line
`V[:5, :5] = V[:5, :5] + 1`):


```python
V = np.zeros((size, n))
print(f"V -->\n{V}\n")

# Changing the values to values from 0 to 24
V[:5, :5] = np.arange(5 * 5).reshape(5, 5)
print('V[:5, :5] = np.arange(5 * 5).reshape(5, 5)\n')
print(f"V[:5, :5] -->\n{V[:5, :5]}\n")
print(f"V -->\n{V}\n")
```

**_Important here:_**

**_The shape of the array you are modifying must match the shape of the array
you are modifying it with:_**


```python
V = np.zeros((size, n))
try:
    V[:5, :5] = np.ones((5, 4))
except Exception as ex:
    print("The previous line did not work!")
    print("Here is the error output:")
    print(f"\t{ex}")
```

Now, we know a little bit better how to manipulate arrays, we want to
initialise the values of the cells at the first time-point at "random" (note
that there is no real random with computers).

To do so we can use the function `random` from `np.random`. The function takes
as an input the size of the table to create for example:


```python
np.random.random((4, 4))
np.random.random?
```

creates a `4*4` array filled with uniformly distributed random numbers between
0 and 1.

> **_To go further_**
>
> For reasons that we will not expose here, it can be mathematically proven
> that any known random distribution can be simulated using a uniform
> distribution in [0, 1), (`)` means the 1 is excluded).
